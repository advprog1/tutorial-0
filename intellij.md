# Tutorial Instalasi Intellij Ultimate

> Author: `ADN`

Intellij memiliki 2 versi aplikasi, yaitu Ultimate dan Community. Namun demikian, kamu diharapkan dapat menggunakan versi Ultimate pada mata kuliah Pemrograman Lanjut ini, karena terdapat beberapa fitur yang tidak didukung pada versi Community.

Untuk bisa mengakses Intellij Ultimate, perlu menggunakan **email UI** untuk mendapat free educational license yang berlaku selama 1 tahun setelah aktivasi.

1. Kunjungi <https://www.jetbrains.com/community/education/#students> <br>

   ![](https://i.imgur.com/Z0BuFi4.png)

2. Scroll hingga ada tombol `Apply Now` dan klik tombol tersebut. <br>

   ![](https://i.imgur.com/V6OB6an.png)

3. Isi data pada form yang ada menggunakan **email UI** dan submit. <br>

   ![](https://i.imgur.com/7KXActV.png)

4. Cek email untuk proses verifikasi dan ikuti petunjuk yang diberikan. <br>

5. Kamu akan diarahkan ke halaman persetujuan lisensi dan diminta untuk mendaftar akun JetBrains. Isi data registrasi pada form yang tersedia. <br>

   ![](https://i.imgur.com/LXKtIWh.png)

6. Setelah akun terdaftar, kamu dapat melihat license yang telah didapatkan pada menu Licenses. Kemudian, pilih Intellij Ultimate IDEA. <br>

   ![](https://i.imgur.com/13fInHJ.png)

7. Unduh versi ultimate, lalu buka file `.exe` dan ikuti petunjuk instalasinya. <br>

   ![](https://i.imgur.com/LQ7GVjj.png)

8. Ketika aplikasi Intellij dibuka, kamu akan diminta untuk login dengan akun JetBrains yang telah dibuat sebelumnya. <br>

   ![](https://i.imgur.com/qRPqkKC.png)

9. Setelah itu, klik tombol `Activate New License` dan Intellij sudah bisa digunakan! 🎉 <br>

   ![](https://i.imgur.com/rUXeRGX.png)
