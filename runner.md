# Tutorial menjalankan Gitlab Runner dengan Docker container

> Author: `FFF`

Gitlab Runner adalah aplikasi yang bertugas untuk mengeksekusi jobs yang ada pada GitLab pipeline. Di bawah ini merupakan tutorial membuat private runners untuk suatu project di GitLab menggunakan Docker container. Seluruh perintah yang ada dilakukan pada Command Prompt/Terminal perangkat kamu. Jika kemampuan perangkat kamu terbatas atau tidak memungkinkan, kamu dapat memanfaatkan layanan cloud seperti GCP dan AWS.

### 1. Melakukan Instalasi Docker
Jika kamu belum pernah menginstall Docker, silahkan [install Docker](https://docs.docker.com/get-docker) terlebih dahulu. Untuk mengeceknya, silakan memeriksa versi Docker yang telah terinstall.
```
docker -v
```

### 2. Mengambil Gitlab Runner Docker Image
```
docker pull gitlab/gitlab-runner
```
> Note: ukuran image sekitar 500 MB

### 3. Membuat Docker Volume
Perintah di bawah ini adalah perintah untuk membuat Docker volume dengan nama **gitlab-runner-config**.
```
docker volume create gitlab-runner-config
```

### 4. Membuat dan Menjalankan Docker Container
Perintah di bawah ini adalah perintah untuk membuat dan menjalankan Docker container dari image **gitlab/gitlab-runner** dengan nama **gitlab-runner**.
```
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```
Daftar semua container dapat dilihat dengan perintah berikut.
```
docker ps -a
```

### 5. Mematikan GitLab Shared Runners
Kamu dapat mematikan shared runners pada bagian **Shared runners** di Settings > CI/CD, lalu expand menu Runners.

### 6. Mendapatkan Registration Token
Kamu dapat memperoleh registration token pada bagian **Specific runners** di Settings > CI/CD, lalu expand menu Runners.

### 7. Mendaftarkan Gitlab Runner
```
docker exec -it gitlab-runner gitlab-runner register
```
Jika kamu bingung dengan **gitlab-runner** yang terlihat berulang, **gitlab-runner** pertama merupakan nama container sedangkan **gitlab-runner** kedua merupakan bagian dari perintah di dalam container tersebut.

```
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.cs.ui.ac.id/

Enter the registration token:
<Registration token project kamu>

Enter a description for the runner:
[<Default Nama Runner>]: <Nama Runner kamu>

Enter tags for the runner (comma-separated):
<Tag Runner kamu (opsional)>

Enter optional maintenance note for the runner:
<Catatan tambahan Runner kamu (opsional)>

Enter an executor: custom, docker, parallels, shell, docker+machine, docker-ssh+machine, instance, docker-ssh, ssh, virtualbox, kubernetes:
docker

Enter the default Docker image (for example, ruby:2.7):
gradle:latest
```

### 8. Mengecek Daftar GitLab Runners
```
docker exec -it gitlab-runner gitlab-runner list
```
Contoh output perintah di atas kurang lebih sebagai berikut.
```
Runtime platform              arch=amd64 os=linux pid=355 revision=12335144 version=15.8.0
Listing configured runners    ConfigFile=/etc/gitlab-runner/config.toml
Advpro runners                Executor=docker Token=sdefa3qxWsdy99HPgdes URL=https://gitlab.cs.ui.ac.id
```

Daftar Runners juga dapat dilihat pada bagian **Available specific runners** di Settings > CI/CD, lalu expand menu Runners.

### 9. Melakukan Unregister GitLab Runner
```
docker exec -it gitlab-runner gitlab-runner unregister -n <nama Runner>
```

### 10. Membuat dan Mendaftarkan Runner Lain
Kamu dapat membuat container baru lagi mulai dari langkah 3 hingga selesai dengan nama volume dan nama container yang berbeda. Jika kamu ingin mendaftarkan runner yang sudah ada ke project lain, cukup mulai dari langkah 5 hingga selesai.

### 11. Mencoba Specific Runner pada Tutorial 0
- Buat file baru dengan nama `.gitlab-ci.yml`.
- Salah satu contoh isi `.gitlab-ci.yml` untuk menjalankan gradle test adalah sebagai berikut.
```yml
image: gradle:7.6-jdk17

variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

stages:
  - test

test:
  stage: test
  script:
    - gradle test --stacktrace
```
- Push kembali ke GitLab dan amati perbedaannya. Kamu dapat masuk ke menu CI/CD untuk melihat jobs yang sedang berjalan.
![](https://i.imgur.com/gcxBedE.png)

Adakalanya penggunaan **shared runners** memiliki beberapa kendala misalnya pada jam-jam sibuk menjelang suatu *deadline*. Hal ini terjadi karena **shared runners** digunakan oleh banyak project sehingga kita harus mengantri dan saling berbagi dengan yang lainnya. Oleh karena itu, **specific runners** dapat kamu coba dan gunakan pada kondisi seperti ini.
