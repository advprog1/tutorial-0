# Git

> author: `TP`

## Apa Itu Git?

Git adalah sebuah program komputer yang digunakan untuk mengelola dan melacak perubahan pada berkas atau proyek. Hal ini berguna ketika kita bekerja pada proyek yang berkelanjutan dan membutuhkan kerja sama dengan programmer lainnya. Git memungkinkan sebuah tim untuk bekerja pada berkas yang sama dan menyimpan versi terbaru dari berkas tersebut, sehingga memudahkan kita untuk dapat memodifikasinya dan menemukan _conflict_ yang terjadi jika berkas tersebut diedit secara bersamaan.

Git juga merupakan salah satu alat pengontrol versi yang paling populer dan banyak digunakan oleh para pengembang perangkat lunak. Dengan Git, setiap perubahan yang dilakukan pada proyek akan dicatat dan disimpan, sehingga pengguna dapat kembali ke versi sebelumnya jika diperlukan. Hal ini juga memungkinkan pengguna untuk dapat membandingkan perubahan antar versi dan membuat cabang-cabang (_branch_) baru. _Branch_ baru bisa digunakan untuk membuat berkas yang bertujuan untuk mempercantik dan menguji fitur baru, sebelum menggabungkannya dengan versi utama dari proyek.

## Istilah-Istilah Dalam Git

Berikut adalah beberapa istilah penting dalam Git:

| Istilah           | Penjelasan                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Repository        | Tempat penyimpanan berkas dan informasi perubahan dalam sebuah proyek                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| Commit            | Aksi menyimpan perubahan pada berkas dalam repository. Setiap commit memiliki pesan yang menjelaskan perubahan apa yang dilakukan                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| Branch            | Cabang versi dari sebuah proyek yang memungkinkan pengguna untuk bekerja pada fitur baru atau memperbaiki masalah tanpa mempengaruhi versi utama dari proyek                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| Merge             | Proses menggabungkan dua atau lebih branch menjadi satu                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Pull Request      | Permintaan untuk menggabungkan branch tertentu ke branch utama. Ini biasanya dilakukan setelah review dan diskusi dengan tim lain                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| HEAD              | Referensi terbaru dalam branch saat ini                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Working Directory | Area tempat kamu menyimpan file-file yang sedang aktif dalam pengembangan proyek kamu. Dalam Git, working directory merupakan folder di sistem file kamu yang berisi semua file proyek dan memungkinkan kamu untuk mengubah, menambahkan, dan menghapus file sebelum di-commit ke dalam repository Git. Setelah file di-commit ke dalam repository, mereka dapat dikelola dan dicatat sejarah perubahannya melalui Git. Working directory dan repository Git bekerja sama untuk memastikan bahwa semua perubahan yang dibuat tercatat dan dapat dikembalikan ke versi sebelumnya jika diperlukan |
| Staging Area      | Tempat transisi untuk berkas yang akan di-commit. Berkas harus di-add ke area ini sebelum di-commit                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| Stash             | Fitur yang memungkinkan kamu untuk menyimpan perubahan pada working directory kamu sebelum kamu beralih ke branch yang berbeda atau melakukan tugas lain. Ini memungkinkan kamu untuk meninggalkan perubahan yang belum selesai dan kemudian kembali dan melanjutkan perubahan tersebut di waktu yang akan datang. Stashing membuat working directory bersih dan hanya mempertahankan perubahan yang sudah di-commit.                                                                                                                                                                            |
| Remote            | Referensi ke repository yang berada di server lain atau hosting service seperti GitHub                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Fork              | Proses membuat salinan repository ke akun pengguna lain                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Clone             | Proses menyalin repository ke komputer lokal pengguna                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |

## Perintah-Perintah Dasar Git

Berikut adalah beberapa perintah dasar Git:

| Perintah                    | Penjelasan                                                                                                                                                                                                                                                                                                                                                                                                                 |
| --------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `git init`                  | Inisialisasi repository Git baru pada direktori saat ini                                                                                                                                                                                                                                                                                                                                                                   |
| `git clone <url>`           | Menyalin repository dari server remote ke komputer lokal. `<url>` merupakan alamat URL repository yang akan diclone                                                                                                                                                                                                                                                                                                        |
| `git add <file>`            | Menambahkan berkas ke daftar perubahan yang akan di-commit. `<file>` bisa diganti dengan nama berkas atau wildcard, seperti tkamu titik (.)                                                                                                                                                                                                                                                                                |
| `git commit -m "<message>"` | Menyimpan perubahan yang telah ditambahkan dengan git add ke repository. `<message>` adalah pesan yang menjelaskan perubahan apa yang dilakukan                                                                                                                                                                                                                                                                            |
| `git status`                | Menampilkan status dari berkas yang ada dalam direktori saat ini dan membedakan antara berkas yang sudah di-commit dan berkas yang belum                                                                                                                                                                                                                                                                                   |
| `git log`                   | Menampilkan riwayat dari semua commit yang pernah dilakukan                                                                                                                                                                                                                                                                                                                                                                |
| `git diff`                  | Menampilkan perbedaan antara versi berkas saat ini dan versi terakhir yang di-commit                                                                                                                                                                                                                                                                                                                                       |
| `git branch <nama>`         | Membuat branch baru dengan nama `<nama>`                                                                                                                                                                                                                                                                                                                                                                                   |
| `git checkout <nama>`       | Berpindah ke branch `<nama>`. Kita juga bisa langsung membuat nama branch baru dan berpindah ke branch baru tersebut dengan perintah `git checkout -b <nama>`                                                                                                                                                                                                                                                              |
| `git merge <nama>`          | Menggabungkan branch`<nama>` ke branch yang sedang aktif                                                                                                                                                                                                                                                                                                                                                                   |
| `git fetch`                 | Mengambil perubahan dari repository remote dan menyimpannya sebagai branch lokal. Ini memungkinkan kamu untuk melihat perubahan yang dibuat oleh orang lain dan mengambil bagian dari mereka tanpa mengintegrasikan perubahan tersebut ke branch utama kamu                                                                                                                                                                |
| `git pull`                  | Mengunduh perubahan terbaru dari repository remote dan menggabungkannya dengan repository lokal                                                                                                                                                                                                                                                                                                                            |
| `git push`                  | Mengirim perubahan dari repository lokal ke repository remote                                                                                                                                                                                                                                                                                                                                                              |
| `git stash`                 | Menyimpan semua perubahan yang belum di-commit ke dalam stash, membuat working directory bersih, dan memungkinkan kamu untuk beralih ke branch yang berbeda atau melakukan tugas lain                                                                                                                                                                                                                                      |
| `git stash apply`           | Memulihkan perubahan yang tersimpan dalam stash kembali ke working directory kamu                                                                                                                                                                                                                                                                                                                                          |
| `git stash drop`            | Menghapus stash                                                                                                                                                                                                                                                                                                                                                                                                            |
| `git pull --rebase`         | Mengambil perubahan dari repository remote dan menerapkannya ke branch lokal kamu sambil memperbarui branch lokal kamu untuk menjadi branch terkini. Ini melakukan `git fetch` untuk mendapatkan perubahan remote, dan kemudian menggabungkan hasilnya dengan branch lokal kamu dengan `git rebase`. Perintah ini membantu menghindari konflik dengan memperbarui branch lokal kamu sebelum menggabungkan perubahan remote |

## Advanced Git

### 1. Git Reset

Git reset adalah perintah Git yang digunakan untuk membatalkan perubahan pada repositori Git. Hal ini dapat digunakan untuk mengambil versi sebelumnya, membatalkan commit, atau membatalkan perubahan pada working tree dan indeks. Git reset memiliki tiga tipe: soft, mixed, dan hard, yang mempengaruhi cara perubahan dalam repositori dibatalkan.

Untuk menggunakan perintah Git reset, kamu dapat menjalankan perintah berikut dalam terminal atau Command Prompt:

```css=
git reset <mode> <commit>
```

Di mana `<mode>` adalah tipe reset (soft, mixed, atau hard), dan `<commit>` adalah SHA1 hash dari commit yang ingin dibatalkan.

Berikut adalah beberapa contoh penggunaan Git reset:

Soft reset:

```code=
git reset --soft HEAD~
```

Perintah ini akan membatalkan commit terakhir dan memindahkan pointer `HEAD` ke commit sebelumnya, tetapi mempertahankan perubahan pada working tree dan indeks.

Mixed reset:

```sh=
git reset HEAD~
```

Perintah ini akan membatalkan commit terakhir dan memindahkan pointer `HEAD` ke commit sebelumnya, tetapi mempertahankan perubahan pada working tree dan menghapus perubahan pada indeks.

Hard reset:

```css=
git reset --hard HEAD
```

Perintah ini akan membatalkan commit terakhir dan memindahkan pointer `HEAD` ke commit sebelumnya, serta menghapus semua perubahan pada working tree dan indeks.

**Pastikan untuk membuat backup dari repositori kamu sebelum menjalankan perintah reset, karena perubahan yang dibatalkan tidak dapat dikembalikan.**

### 2. Git Revert

Git revert adalah perintah Git yang digunakan untuk membatalkan perubahan dalam repositori Git dengan cara membuat commit baru yang membatalkan perubahan pada commit sebelumnya. Ini berbeda dari git reset yang secara permanen menghapus perubahan dalam repositori. Git revert memungkinkan kamu untuk membatalkan perubahan tanpa kehilangan sejarah perubahan pada repositori kamu.

Untuk menggunakan perintah Git revert, kamu dapat menjalankan perintah berikut dalam terminal atau Command Prompt:

```php=
git revert <commit>
```

Di mana `<commit>` adalah SHA1 hash dari commit yang ingin dibatalkan. Setelah perintah ini dijalankan, Git akan membuat commit baru yang mengambil alih efek dari commit yang dibatalkan.

Pastikan untuk membuat backup dari repositori kamu sebelum menjalankan perintah revert, meskipun perubahan yang dibatalkan tidak akan dihapus secara permanen.

Berikut adalah contoh penggunaan perintah Git revert:

```python=
# Menentukan commit yang ingin dibatalkan
git log

# Copy SHA1 hash dari commit yang ingin dibatalkan
git revert <commit-hash>

# Melakukan commit baru yang membatalkan perubahan
git commit -m "Revert commit <commit-hash>"
```

Perintah ini akan membuat commit baru yang membatalkan perubahan yang diterapkan oleh commit dengan mengambil identifier-nya melalui SHA1 hash `<commit-hash>`. Setelah itu, kamu dapat melakukan push ke remote repository untuk membagikan perubahan.

### 3. Git Cherry-Pick

Git cherry-pick adalah perintah Git yang digunakan untuk memilih dan menerapkan perubahan dari satu branch ke branch lain. Ini memungkinkan kamu untuk memilih commit tertentu dan mengaplikasikannya ke branch saat ini tanpa harus memperluas branch dan merge.

Untuk menggunakan perintah Git cherry-pick, kamu dapat menjalankan perintah berikut dalam terminal atau Command Prompt:

```php=
git cherry-pick <commit>
```

Di mana `<commit>` adalah SHA1 hash dari commit yang ingin dipilih. Setelah perintah ini dijalankan, Git akan menerapkan perubahan yang diterapkan oleh commit ke branch saat ini.

Pastikan untuk membuat backup dari repositori kamu sebelum menjalankan perintah cherry-pick, meskipun perubahan yang diterapkan tidak akan dihapus secara permanen.

### 4. Git Rebase

Git rebase adalah perintah Git yang digunakan untuk memodifikasi sejarah branch. Ini memungkinkan kamu untuk mengubah posisi branch saat ini sehingga menjadi anak dari branch lain atau branch baru. Git rebase juga memungkinkan kamu untuk menggabungkan perubahan dari branch lain ke branch saat ini dengan memindahkan commit branch saat ini ke atas perubahan branch lain.

Untuk menggunakan perintah Git rebase, kamu dapat menjalankan perintah berikut dalam terminal atau Command Prompt:

```php=
git rebase <upstream>
```

Di mana `<upstream>` adalah nama branch atau remote repository yang akan digunakan sebagai dasar untuk rebase. Setelah perintah ini dijalankan, Git akan memindahkan branch saat ini ke atas perubahan dari `<upstream>` dan menerapkan perubahan dari branch saat ini ke atas perubahan dari `<upstream>`.

Perintah Git rebase sangat berguna untuk menjaga sejarah branch yang bersih dan mudah dipahami. Namun, pastikan untuk membuat backup dari repositori kamu sebelum menjalankan perintah rebase, karena perubahan yang diterapkan oleh rebase dapat mengubah sejarah perubahan branch dan menyebabkan _conflict_ perubahan.
